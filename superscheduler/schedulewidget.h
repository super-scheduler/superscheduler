#ifndef SCHEDULEWIDGET_H
#define SCHEDULEWIDGET_H

#include <QWidget>
#include <QPushButton>

namespace Ui {
class ScheduleWidget;
}

class ScheduleWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ScheduleWidget(QWidget *parent = nullptr);
    ~ScheduleWidget();

    QPushButton* getLogOutButton();

private:
    Ui::ScheduleWidget *ui;
};

#endif // SCHEDULEWIDGET_H
