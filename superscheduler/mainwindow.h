#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "authwidget.h"
#include "schedulewidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void onAuthButtonClicked();
    void onRegButtonClicked();
    void onLogOutButtonClicked();

private:
    Ui::MainWindow *ui;
    AuthWidget* m_authWidget;
    ScheduleWidget* m_scheduleWidget;

    int m_count = 0;
    bool isAuth = true;
};
#endif // MAINWINDOW_H
