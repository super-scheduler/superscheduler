#include "schedulewidget.h"
#include "ui_schedulewidget.h"

ScheduleWidget::ScheduleWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ScheduleWidget)
{
    ui->setupUi(this);
}

ScheduleWidget::~ScheduleWidget()
{
    delete ui;
}

QPushButton *ScheduleWidget::getLogOutButton()
{
    return ui->pushButtonLogOut;
}
